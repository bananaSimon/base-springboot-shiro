package com.mx1014.rsd.controller.web;


import com.mx1014.rsd.controller.BaseController;
import com.mx1014.rsd.entity.User;
import com.mx1014.rsd.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class IndexController extends BaseController {

    @Autowired
    private IUserService userService;

    private Logger logger = LoggerFactory.getLogger(getClass());

    @RequestMapping(value = {"/", "/index"})
    public String index() {
        List<User> users = userService.findAll();
        logger.debug(users.toString());
        return "index";
    }
}
