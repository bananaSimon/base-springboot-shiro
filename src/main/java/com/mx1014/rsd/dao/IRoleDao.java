package com.mx1014.rsd.dao;

import com.mx1014.rsd.dao.support.IBaseDao;
import com.mx1014.rsd.entity.Role;
import org.springframework.stereotype.Repository;

@Repository
public interface IRoleDao extends IBaseDao<Role, Integer> {

}
