package com.mx1014.rsd.dao;

import com.mx1014.rsd.dao.support.IBaseDao;
import com.mx1014.rsd.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserDao extends IBaseDao<User, Integer> {

    User findByUserName(String username);

}
